let colors = [1, 2, 3, 4, 5, 6, "A", "B", "C", "D", "E"];
let btn = document.querySelector(".btn");
let color_code = document.querySelector(".color_code");

btn.addEventListener("click", function () {
  let hexColor = "#";
  for (let i = 0; i < 6; i++) {
    hexColor += colors[random_c()];
  }
  color_code.textContent = hexColor;
  document.body.style.backgroundColor = hexColor;
});

function random_c() {
  return Math.floor(Math.random() * colors.length);
}
