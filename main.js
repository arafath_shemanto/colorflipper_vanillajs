let colors = ["green", "red", "#f0f0f0", "rgba(0, 0, 0, 0.1)"];
let btn = document.querySelector(".btn");
let code = document.querySelector(".color_code");

btn.addEventListener("click", function () {
  let random_color = getRandomColor();
  code.textContent = colors[random_color];
  document.body.style.backgroundColor = colors[random_color];
});

function getRandomColor() {
  return Math.floor(Math.random() * colors.length);
}
